/*
 * GAME SCENE
 * Copyright © 2020 David Martín Almazán
 */

#include "Game_Scene.hpp"

#include <cstdlib>
#include <basics/Canvas>
#include <basics/Director>

using namespace basics;
using namespace std;


namespace example
{
    // ---------------------------------------------------------------------------------------------
    /// ID y ruta de las texturas que se deben cargar para esta escena. La textura con el mensaje de
    /// carga está la primera para poder dibujarla cuanto antes:

    Game_Scene::Texture_Data Game_Scene::textures_data[] =
    {
        { ID(loading),          "game-scene/loading.png"            },
        { ID(color_red),        "red.png"                           },
        { ID(color_blue),       "blue.png"                          },
        { ID(color_yellow),     "yellow.png"                        },
        { ID(color_green),      "green.png"                         },
        { ID(victory),          "victoria.png"                      },
        { ID(pause),            "boton_pause.png"                   },
        { ID(PauseMenu),        "menu_pause.png"                    },


    };

    /// Para determinar el número de items en el array textures_data, se divide el tamaño en bytes del array completo entre el tamaño en bytes de un item:

    unsigned Game_Scene::textures_count = sizeof(textures_data) / sizeof(Texture_Data);

    // ---------------------------------------------------------------------------------------------

    Game_Scene::Game_Scene()
    {
        // Se establece la resolución virtual (independiente de la resolución virtual del dispositivo).
        // En este caso no se hace ajuste de aspect ratio, por lo que puede haber distorsión cuando
        // el aspect ratio real de la pantalla del dispositivo es distinto.

        canvas_width  = 1280;
        canvas_height =  720;

        // Se inicializan otros atributos:

        initialize ();
    }

    // ---------------------------------------------------------------------------------------------
    // Algunos atributos se inicializan en este método en lugar de hacerlo en el constructor porque
    // este método puede ser llamado más veces para restablecer el estado de la escena y el constructor
    // solo se invoca una vez.

    bool Game_Scene::initialize ()
    {
        state = LOADING;
        suspended = false;
        gameplay  = UNINITIALIZED;

        return true;
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::suspend ()
    {
        suspended = true;               // Se marca que la escena ha pasado a primer plano
        /*
        menuPause->show();
        if(arrow->intersects(*menuPause))
            resume();
            */
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::resume ()
    {
        suspended = false;              // Se marca que la escena ha pasado a segundo plano
        //menuPause->hide();
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::handle (Event & event) {
        if (state == RUNNING)               // Se descartan los eventos cuando la escena está LOADING
        {
            if (gameplay == WAITING_TO_START)
            {
                start_playing();           // Se empieza a jugar cuando el usuario toca la pantalla por primera vez
            }
            else switch (event.id)
            {
                case ID(touch - started):     // El usuario toca la pantalla
                case ID(touch - moved): {

                    user_target_y = *event[ID(y)].as<var::Float>();  //Se guarda la coordenada Y de donde ha tocado el jugador.
                    user_target_x = *event[ID(x)].as<var::Float>();  //Se guarda la coordenada X de donde ha tocado el jugador.




                    if(PuedePulsar) //Si puede pulsar
                    {
                        follow_target = true; //Follow_target se pone a true, y el jugador puede interactuar con la mecánica principal del juego
                    }
                    break;
                }

                case ID(touch - ended):       // El usuario deja de tocar la pantalla
                {
                    follow_target = false;    //Follow_target se pone a false
                    break;
                }
            }
        }
    }


    //--------------------------------------------------------------------------
    void Game_Scene::MenuPause ()
    {
        menuPause->show();
        suspended = true;

        Point2f click = {user_target_x, user_target_y};
        if(pauseButton->contains(click))
        {
            menuPause->hide();
            suspended = false;
        }
    }
    // ---------------------------------------------------------------------------------------------

    void Game_Scene::update (float time)
    {
        if (!suspended)
        {
            switch (state)
            {
                case LOADING: load_textures  ();     break;
                case RUNNING: run_simulation (time); break;
                case ERROR:   break;
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::render (Graphics_Context::Accessor & context)
    {
        if (!suspended)
        {
            // El canvas se puede haber creado previamente, en cuyo caso solo hay que pedirlo:

            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            // Si no se ha creado previamente, hay que crearlo una vez:

            if (!canvas)
            {
                 canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if(canvas)
            {
                canvas->clear();

                switch (state)
                {
                    case LOADING: render_loading(*canvas); break;  //Si está en loading, se carga primero este sprite
                    case RUNNING: render_playfield (*canvas); break; //Se carga lo que sería la pantalla principal (los sprites/assets)
                    case ERROR:   break;
                }
            }
        }
    }

    void Game_Scene::load_textures ()
    {
        if (textures.size () < textures_count)          // Si quedan texturas por cargar...
        {
            // Las texturas se cargan y se suben al contexto gráfico, por lo que es necesario disponer
            // de uno:

            Graphics_Context::Accessor context = director.lock_graphics_context ();

            if (context)
            {
                // Se carga la siguiente textura (textures.size() indica cuántas llevamos cargadas):

                Texture_Data   & texture_data = textures_data[textures.size ()];
                Texture_Handle & texture      = textures[texture_data.id] = Texture_2D::create (texture_data.id, context, texture_data.path);

                // Se comprueba si la textura se ha podido cargar correctamente:

                if (texture) context->add (texture); else state = ERROR;

                // Cuando se han terminado de cargar todas las texturas se pueden crear los sprites que
                // las usarán e iniciar el juego:
            }
        }
        else
        if (timer.get_elapsed_seconds () > 1.f)         // Si las texturas se han cargado muy rápido
        {                                               // se espera un segundo desde el inicio de
            create_sprites ();                          // la carga antes de pasar al juego para que
            restart_game   ();                          // el mensaje de carga no aparezca y desaparezca
                                                        // demasiado rápido.
            state = RUNNING;
        }
    }

///Para importar los sprites, y que se queden guardados, en el momento en el que falle uno, dará un error, y se pondrá la pantalla en negro.
    void Game_Scene::create_sprites ()
    {
        /// Se crean y configuran los sprites del fondo:

        Sprite_Handle    red_color(new Sprite( textures[ID(   color_red)].get () ));
        Sprite_Handle   blue_color(new Sprite( textures[ID(  color_blue)].get () ));
        Sprite_Handle  green_color(new Sprite( textures[ID( color_green)].get () ));
        Sprite_Handle yellow_color(new Sprite( textures[ID(color_yellow)].get () ));

        sprites.push_back (red_color);
        sprites.push_back (green_color);
        sprites.push_back (yellow_color);
        sprites.push_back (blue_color);

        Sprite_Handle   Victory_Handle(new Sprite (textures[ID(victory)].get() ));

        Victory_Handle->set_position({canvas_width*.5f, canvas_height*.5f});

        sprites.push_back (Victory_Handle);

        Sprite_Handle      pauseIcon(new Sprite (textures[ID(pause)].get() ));
        Sprite_Handle  MenuPausa(new Sprite (textures[ID(PauseMenu)].get() ));

        pauseIcon->set_scale(0.3);
        pauseIcon->set_position ({100.f, 100.f});
        MenuPausa->set_position ({canvas_width*.5f, canvas_height*.5f});

        sprites.push_back (pauseIcon);
        sprites.push_back (MenuPausa);

        /// Se guardan punteros a los sprites que se van a usar frecuentemente:

        red            =               red_color.get ();
        green          =             green_color.get ();
        blue           =              blue_color.get ();
        yellow         =            yellow_color.get ();
        victory        =          Victory_Handle.get ();
        pauseButton    =               pauseIcon.get ();
        menuPause      =               MenuPausa.get ();

        /// Se esconde los siguientes elementos.
        menuPause->hide();
        victory->hide();
    }


    // ---------------------------------------------------------------------------------------------
    /// Juando el juego se inicia por primera vez o cuando se reinicia porque un jugador pierde, se
    /// llama a este método para restablecer la posición y velocidad de los sprites:

    void Game_Scene::restart_game()
    {
        //Ponemos en posición los sprites, y como tienen distintos tamaños, se calcula la escala para que tengan aproximadamente el mismo tamaño
        red->set_scale   (0.5);
        red->set_position ({ canvas_width*.3f, canvas_height*.75f });

        yellow->set_scale(1.25);
        yellow->set_position ({ canvas_width*.3f , canvas_height*.25f });

        blue->set_position({canvas_width*.6f, canvas_height*.75f});

        green->set_scale(0.4);
        green->set_position({canvas_width*.6f, canvas_height*.25f});

        //Ponemos el nivel a 0
        nivel = 0;

        gameplay = WAITING_TO_START;
    }

    // ---------------------------------------------------------------------------------------------

    ///Ponemos el estado de gameplay a playing, y así empezar a ciclar la mecánica
    void Game_Scene::start_playing ()
    {
        gameplay = PLAYING;
    }

    // ---------------------------------------------------------------------------------------------

    ///Corre la simulación donde si crearSecuencia es falso, se crea una secuencia, y si es verdadero
    ///La pinta por pantalla.
    void Game_Scene::run_simulation (float time)
    {
        for(auto & sprite:sprites)
        {
            sprite->update (time);
        }

        if(!crearSecuencia)
            originarSecuencia ();
        else
            create_secuence();

        update_user();
    }

    // ---------------------------------------------------------------------------------------------
    // Se pinta por pantalla la secuencia creada con anterioridad

    void Game_Scene::create_secuence ()
    {
        //Un switch en vez de unos if's para que resulte más ordenado y óptimo, y recoge el número que hay en la posición (depende del nivel) del array de la secuencia
        switch(secuencia[nivel])
        {
            case 0:
                yellow->hide();
                break;
            case 1:
                red->hide();
                break;
            case 2:
                green->hide();
                break;
            case 3:
                blue->hide();
                break;
        }

        //Pasa un tiempo, y se vuelven a mostrar

        if (timer.get_elapsed_seconds () > TimeLevel)
        {
            yellow->show();
            red->show();
            green->show();
            blue->show();
        }

        //Si es más del nivel 10, el tiempo desciende para que le resulté más difícil al jugador.
        if(nivel==10)
            TimeLevel = TimeLevel-1.f;


        PuedePulsar = true;

    }

    // ---------------------------------------------------------------------------------------------
    // Se crea un array de longitud 20, cada uno corresponde con un nivel y se le añade a esa posicion un numero aleatorio del 0 al 3

    void Game_Scene::originarSecuencia ()
    {
        for(int i = 0; i < secuencia_length; ++i)
        {
            boton_secuencia = rand()%4;
            secuencia[i] = boton_secuencia;
        }
        crearSecuencia = true;
    }

    ///La interacción con el jugador, aquí se guarda que es lo que ha pulsado, haciendo que interseccionen el array del puntero con el color
    void Game_Scene::update_user ()
    {
        if(follow_target)
        {
            Point2f click = {user_target_x, user_target_y};

            if(pauseButton->contains(click))  //Si el sprite de arrow, contacta con el puntero al que hace referencia el sprite de pause
            {
                MenuPause(); //Se muestra el spirte de menuPause que estaba oculto (visible = true)
            }

            if(yellow->contains(click))
                color_elegido = 0;
            else if(red->contains(click))
                color_elegido = 1;
            else if(green->contains(click))
                color_elegido = 2;
            else if(blue->contains(click))
                color_elegido = 3;

            PuedePulsar = false;
            Comprobar_Color();
        }
    }

    ///Comprobar color, es que ve si se ha elegido bien, si está bien, se mira el nivel, si esta mal se vuelve al inicio
    void Game_Scene::Comprobar_Color() {
        if(color_elegido==secuencia[nivel])
        {
            //Si no menor que level 20, se suma uno
            if(nivel<20)
            {
                nivel ++;
                create_secuence();
            }
            //Si es 20 o más, se pinta la pantalla de victoria
            else
            {
                victory->show();
            }
        }
        else
            restart_game();

    }

    ///Simplemente se dibuja el sprite que conforma el loading
    void Game_Scene::render_loading (Canvas & canvas)
    {
        Texture_2D * loading_texture = textures[ID(loading)].get ();

        if (loading_texture)
        {
            canvas.fill_rectangle
                    (
                            { canvas_width * .5f, canvas_height * .5f },
                            { loading_texture->get_width (), loading_texture->get_height () },
                            loading_texture
                    );
        }
    }

    // ---------------------------------------------------------------------------------------------
    /// Simplemente se dibujan todos los sprites que conforman la escena.
    void Game_Scene::render_playfield (Canvas & canvas)
    {
        for (auto & sprite : sprites)
        {
            sprite->render (canvas);
        }
    }

}
