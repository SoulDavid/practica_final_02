/*
 * GAME SCENE
 * Copyright © 2020 David Martín Almazán
 */

#ifndef GAME_SCENE_HEADER
#define GAME_SCENE_HEADER

    #include <map>
    #include <list>
    #include <memory>

    #include <basics/Canvas>
    #include <basics/Scene>
    #include <basics/Texture_2D>
    #include <basics/Timer>
    #include <basics/Atlas>
    #include <basics/Raster_Font>
    #include <basics/Point>
    #include <basics/Size>
    #include <cstdlib>
    #include <iostream>

    #include "Sprite.hpp"

    namespace example
    {

        using basics::Id;
        using basics::Timer;
        using basics::Canvas;
        using basics::Atlas;
        using basics::Point2f;
        using basics::Size2f;
        using basics::Texture_2D;


        class Game_Scene : public basics::Scene
        {

            // Estos typedefs pueden ayudar a hacer el código más compacto y claro:

            std::unique_ptr< basics::Raster_Font> Font;
            typedef std::shared_ptr < Sprite     >     Sprite_Handle;
            typedef std::list< Sprite_Handle     >     Sprite_List;
            typedef std::shared_ptr< Texture_2D  >     Texture_Handle;
            typedef std::map< Id, Texture_Handle >     Texture_Map;

            typedef basics::Graphics_Context::Accessor Context;

            /**
             * Representa el estado de la escena en su conjunto.
             */
            enum State
            {
                LOADING,
                RUNNING,
                ERROR
            };

            /**
             * Representa el estado del juego cuando el estado de la escena es RUNNING.
             */
            enum Gameplay_State
            {
                UNINITIALIZED,
                WAITING_TO_START,
                PLAYING
            };

        private:

            /**
             * Array de estructuras con la información de las texturas (Id y ruta) que hay que cargar.
             */
            static struct   Texture_Data { Id id; const char * path; } textures_data[];

            /**
             * Número de items que hay en el array textures_data.
             */
            static unsigned textures_count;

        private:

            static constexpr unsigned secuencia_length = 20;   //La longitud del array de secuencia (simulando los niveles que hay)
            int secuencia[secuencia_length];                   //Guarda la secuencia
            int boton_secuencia;                               //El numero aleatorio, que corresponde con el color al que tiene que pulsar el jugador
            int color_elegido;                                 //El color que ha pulsado
            int nivel = 0;                                     //El nivel en el que se encuentra

        private:

            State          state;                               ///< Estado de la escena.
            Gameplay_State gameplay;                            ///< Estado del juego cuando la escena está RUNNING.
            bool           suspended;                           ///< true cuando la escena está en segundo plano y viceversa.
            bool           PuedePulsar = false;

            unsigned       canvas_width;                        ///< Ancho de la resolución virtual usada para dibujar.
            unsigned       canvas_height;                       ///< Alto  de la resolución virtual usada para dibujar.

            Texture_Map    textures;                            ///< Mapa  en el que se guardan shared_ptr a las texturas cargadas.
            Sprite_List    sprites;                             ///< Lista en la que se guardan shared_ptr a los sprites creados.



            Sprite       * red;                                 ///< Puntero al sprite de la lista de sprites que representa el color rojo.
            Sprite       * blue;                                ///< Puntero al sprite de la lista de sprites que representa el color azul.
            Sprite       * green;                               ///< Puntero al sprite de la lista de sprites que representa el color verde.
            Sprite       * yellow;                              ///< Puntero al sprite de la lista de sprites que representa el color amarillo.
            Sprite       * arrow;                               ///< Puntero al sprite de la lista de sprites que representa el puntero.
            Sprite       * victory;                             ///< Puntero al sprite de la lista de sprites que representa la victoria.
            Sprite       * pauseButton;                         ///< Puntero al sprite de la lista de sprites que representa el botón de pausa.
            Sprite       * menuPause;                           ///< Puntero al sprite de la lista de sprites que representa el menu pausa

            Timer          timer;                               ///< Cronómetro usado para medir intervalos de tiempo

            float          TimeLevel=2.f;                       ///< El tiempo que le da al jugador para que vea que color debe pulsar
            bool           follow_target;                       ///< true si el usuario está tocando la pantalla y el jugador pueda tocar pantalla.
            float          user_target_y;                       ///< Coordenada Y donde toca el jugador
            float          user_target_x;                       ///< Coordenada X donde toca el jugador
            bool           crearSecuencia= false;               ///< Booleano para que no este todo el rato creando una secuencia


        public:

            /**
             * Solo inicializa los atributos que deben estar inicializados la primera vez, cuando se
             * crea la escena desde cero.
             */
            Game_Scene();

            /**
             * Este método lo llama Director para conocer la resolución virtual con la que está
             * trabajando la escena.
             * @return Tamaño en coordenadas virtuales que está usando la escena.
             */
            basics::Size2u get_view_size () override
            {
                return { canvas_width, canvas_height };
            }

            /**
             * Aquí se inicializan los atributos que deben restablecerse cada vez que se inicia la escena.
             * @return
             */
            bool initialize () override;

            /**
             * Este método lo invoca Director automáticamente cuando el juego pasa a segundo plano.
             */
            void suspend () override;

            /**
             * Este método lo invoca Director automáticamente cuando el juego pasa a primer plano.
             */
            void resume () override;

            /**
             * Este método se invoca automáticamente una vez por fotograma cuando se acumulan
             * eventos dirigidos a la escena.
             */
            void handle (basics::Event & event) override;

            /**
             * Este método se invoca automáticamente una vez por fotograma para que la escena
             * actualize su estado.
             */
            void update (float time) override;

            /**
             * Este método se invoca automáticamente una vez por fotograma para que la escena
             * dibuje su contenido.
             */
            void render (Context & context) override;

        private:

            /**
             * En este método se cargan las texturas (una cada fotograma para facilitar que la
             * propia carga se pueda pausar cuando la aplicación pasa a segundo plano).
             */
            void load_textures ();

            /**
             * En este método se crean los sprites cuando termina la carga de texturas.
             */
            void create_sprites ();

            /**
             * Se llama cada vez que se debe reiniciar el juego. En concreto la primera vez y cada
             * vez que un jugador pierde.
             */
            void restart_game ();

            /**
             * Cuando se ha reiniciado el juego y el usuario toca la pantalla por primera vez se
             * pone la bola en movimiento en una dirección al azar.
             */
            void start_playing ();

            /**
             * Actualiza el estado del juego cuando el estado de la escena es RUNNING.
             */
            void run_simulation (float time);

            /**
             * Pinta por pantalla el color
             */
            void create_secuence ();

            /**
             * Se crea los distintos niveles del jeugo, dandole un numero que corresponde con el color que tiene que pintar en ese nivel
             */
            void originarSecuencia ();


            /**
             * Dibuja la textura con el mensaje de carga mientras el estado de la escena es LOADING.
             * La textura con el mensaje se carga la primera para mostrar el mensaje cuanto antes.
             * @param canvas Referencia al Canvas con el que dibujar la textura.
             */
            void render_loading (Canvas & canvas);

            /**
             * Dibuja la escena de juego cuando el estado de la escena es RUNNING.
             * @param canvas Referencia al Canvas con el que dibujar.
             */
            void render_playfield (Canvas & canvas);

            /**
             * Para ver si ha acertado o fallado el jugador
             */
            void Comprobar_Color ();


            /**
             * El turno del jugador
             */
            void update_user();

            /**
             * Menu de Pause
             */
            void MenuPause();

        };

    }

#endif
